<?php
/**
 * Test the basic queue functionality.
 */
class GearmanQueueTestCase extends DrupalWebTestCase {
  public static function getInfo() {
    return array(
      'name' => 'Gearman Queue functionality',
      'description' => 'Queues and dequeues a set of items to check the basic queue functionality.',
      'group' => 'Gearman',
    );
  }

  function setUp() {
    parent::setUp(array('gearman', 'gearman_test'));
  }

  /**
   * Queues a set of items to check the basic Gearman queue functionality.
   */
  function testQueue() {
    // Create a Gearman queue.
    $queue_name = $this->randomName();
    variable_set('queue_class_' . $queue_name, 'DrupalGearmanQueue');
    $queue1 = DrupalQueue::get($queue_name);
    $queue1->createQueue();

    // Create four items.
    $data = array();
    for ($i = 0; $i < 4; $i++) {
      $o = new stdClass();
      $o->{$this->randomName()} = $this->randomName();
      $data[] = $o;
    }

    // Queue items 1 and 2 in the queue1.
    $queue1->createItem($data[0]);
    $queue1->createItem($data[1]);


    // Create our worker object.
    $gmworker= new GearmanWorker();

    // Connect to our server(s)
    $servers = variable_get('gearman_server', 'localhost:4730');
    $gmworker->addServers($servers);
    $gmworker->setTimeout(100);

    // Register callback to collect item from our Gearman queue.
    $collector = new stdClass();
    $collector->items = array();
    $gmworker->addFunction($queue_name, function(GearmanJob $job) use ($collector) {
      $collector->items[] = json_decode($job->workload())->data;
    });

    // Process all available itenms in the queue.
    while($gmworker->work()) {
      if ($gmworker->returnCode() != GEARMAN_SUCCESS) {
        break;
      }
    }

    $this->assertEqual(count($collector->items), 2, 'Two items have been retrieved from Gearman.');
    $this->assertEqual($data[0], $collector->items[0]);
    $this->assertEqual($data[1], $collector->items[1]);

    // Queue items 3 and 4 in the queue1.
    $queue1->createItem($data[2]);
    $queue1->createItem($data[3]);

    // Process all available itenms in the queue.
    while($gmworker->work()) {
      if ($gmworker->returnCode() != GEARMAN_SUCCESS) {
        break;
      }
    }

    $this->assertEqual(count($collector->items), 4, 'Four items have been retrieved from Gearman.');
    $this->assertEqual($data[2], $collector->items[2]);
    $this->assertEqual($data[3], $collector->items[3]);

  }

  function testCronQueue() {
    // Retrieve the test queue.
    $queue = DrupalQueue::get('gearman_test_cron_queue');

    $this->assertTrue($queue instanceof DrupalGearmanQueue, 'The queue object for gearman_test_cron_queue is a DrupalGearmanQueue instance.');

    // Enqueue an item into cron queue.
    $data = $this->randomString();
    $queue->createItem($data);

    // Check that the item is not processed by a cron run.
    $this->cronRun();
    $this->assertNotEqual($data, variable_get('gearman_test_cron_queue', NULL), 'Item not processed by cron run.');

    // Check that the test queue is seen by our Drush worker.
    $this->assertGermanFunctionIsImplemented('gearman_test_cron_queue', 'Test cron queue is implemented as a Gearman queue.');

    // Create our worker object.
    $gmworker= new GearmanWorker();

    // Connect to our server(s)
    $servers = variable_get('gearman_server', 'localhost:4730');
    $gmworker->addServers($servers);
    $gmworker->setTimeout(100);

    // Register functions.
    $functions = module_invoke_all('gearman_drush_function');
    foreach ($functions as $fn) {
      $fn += array(
        'timeout' => 0,
        'context' => NULL,
      );
      $gmworker->addFunction($fn['function_name'], $fn['function'], $fn['context'], $fn['timeout']);
    }

    // Process all available itenms in the queue.
    while($gmworker->work()) {
      if ($gmworker->returnCode() != GEARMAN_SUCCESS) {
        break;
      }
    }

    // Check that the item is processed by a Gearman worker.
    $this->assertEqual($data, variable_get('gearman_test_cron_queue', NULL), 'Item processed by cron run.');
  }


  /**
   * Helper function to assert a (Gearman) function is implemented by a module.
   *
   * @param $function_name
   * @param null $message
   * @param string $group
   * @return mixed
   */
  function assertGermanFunctionIsImplemented($function_name, $message = NULL, $group = 'Other') {
    if ($message === NULL) {
      $message = "$function_name handled by drush Gearman workker.";
    }

    // Pick up gearman functions implemented in other modules.
    $functions = module_invoke_all('gearman_drush_function');

    foreach ($functions as $fn) {
      if ($fn['function_name'] === $function_name) {
        // Pass.
        $this->assert('pass', $message, $group);
        // Exit function.
        return;
      }
    }
    // Fail
    $this->assert('fail', $message, $group);
  }

}