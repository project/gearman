
ABOUT
-----

This module provide Gearman integration for Drupal.


OVERVIEW
--------

Gearman provides a generic application framework to farm out work to
other machines or processes that are better suited to do the work. It
allows you to do work in parallel, to load balance processing, and to
call functions between languages. It can be used in a variety of
applications, from high-availability web sites to the transport of
database replication events. In other words, it is the nervous system
for how distributed processing communicates.

FEATURES
--------
 - Gearman backend for Drupal's Queue API. Allowing application to enqueue item
   to be processed by Gearman workers using Drupal's Queue API. The queue is
   read-only, item can be enqueued but not processed using Drupal's API. Items
   processing require a Gearman worker able to execute the queue items as
   intended.
 - Drush worker to process Gearman queue item. Third party module can provide
   Gearman functions with implementations of hook_gearman_drush_function().
 - Drush client to enqueue item using Drush as CLI.
 - Gearmanization of cron queues with the Drush worker.


DRUSH
-----

You can get a feeling for the utility of the gearman workflow quickly by
using the provided Drush examples. After installing the module and
configuring it to connect to your Gearmand job queue, start up a drush 
worker by running "drush gearman-worker". Then, in a second terminal 
you can send commands to the queue which will be processed by the worker.

Two example job functions are provided:

1) "reverse" - the classic gearman example:

   $> drush gearman-client reverse "reverse my text"

2) "drush-invoke" - a proof of concept allowing you to run other drush 
   commands via the persistent worker thread:
   
   $> drush gearman-client drush-invoke status
   $> drush gearman-client drush-invoke "cc all"
   
Your specific implementation will probably lead you to create your own 
job types and callbacks, which you can declare by implementing 
hook_gearman_drush_function().


TODO
----

Current Drush worker does not include much features and does not try to be a
proper daemonized process. The Drush daemon library provides a generic solution
to run Drush as a daemom. It could be used to provide a better Drush worker
(see https://drupal.org/node/1608456). Another solution would be to have a
non-daemonized Drush command to be run periodically as a cron job.


LINKS
-----

 * Gearman: http://gearman.org
 * Gearman PECL extension: http://us2.php.net/manual/en/book.gearman.php
 * Overview of setup and configuration of Gearman
   http://toys.lerdorf.com/archives/51-Playing-with-Gearman.html
